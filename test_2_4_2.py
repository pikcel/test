import os
from selenium.webdriver.chrome.webdriver import WebDriver
import time
import math

from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


def test_2_4_2():
    link = 'http://suninjuly.github.io/explicit_wait2.html'  # url для перехода
    try:
        driver = WebDriver(executable_path='C://Users//Tom//Documents//WebUI//chromedriver.exe')
        driver.get(link)
        #жду в течении 12 сек пока в блоке с id='price' появится цена 100$ и кликаю на кнопку book
        WebDriverWait(driver, 12).until(
            EC.text_to_be_present_in_element((By.ID, 'price'), '$100')
        )
        driver.find_element_by_id('book').click()

        #скроллю страницу до кнопки
        button = driver.find_element_by_id('solve')
        driver.execute_script('return arguments[0].scrollIntoView(true);', button)

        # вытягиваю значение для Х и расчитываю формулу
        x = driver.find_element_by_id('input_value').text
        def calc(x):
            return str(math.log(abs(12 * math.sin(int(x)))))
        y = calc(x)
        driver.find_element_by_id('answer').send_keys(y)
        button.click()

        #получаю ответ с алерта
        alert = driver.switch_to.alert
        alert_text = alert.text

        print(alert_text)

    finally:
        time.sleep(5)
        driver.quit()