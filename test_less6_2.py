from selenium.webdriver.chrome.webdriver import WebDriver
import time
import math

def test_less6_2():
    link = 'http://suninjuly.github.io/find_link_text'  # url для перехода
    try:
        driver = WebDriver(executable_path='C://Users//Tom//Documents//WebUI//chromedriver.exe')
        driver.get(link)
        # поиск ссылки по математическому решению и переход по ссылке
        examples = str(math.ceil(math.pow(math.pi, math.e)*10000))
        link = driver.find_element_by_partial_link_text(examples)
        link.click()
        # заполнение формы новой страницы
        input1 = driver.find_element_by_name('first_name')
        input1.send_keys('First name')

        input2 = driver.find_element_by_name('last_name')
        input2.send_keys('Last name')

        input3 = driver.find_element_by_class_name('city')
        input3.send_keys('Kyiv')

        input4 = driver.find_element_by_id('country')
        input4.send_keys('Ukraine')

        button = driver.find_element_by_class_name('btn.btn-default')
        button.click()
    finally:
        time.sleep(20)
        driver.quit()