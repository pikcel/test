import os
from selenium.webdriver.chrome.webdriver import WebDriver
import time
import math
from selenium.webdriver.support.select import Select


def test_2_2_3_sendfile():
    link = 'http://suninjuly.github.io/file_input.html'  # url для перехода
    try:
        driver = WebDriver(executable_path='C://Users//Tom//Documents//WebUI//chromedriver.exe')
        driver.get(link)
        # заполняю форму
        input1 = driver.find_element_by_name('firstname').send_keys('First name')
        input2 = driver.find_element_by_name('lastname').send_keys('Last name')
        input3 = driver.find_element_by_name('email').send_keys('test@test.com')
        #в директории в котороый находится мой тест файл .ру ищу текстовый файл который хочу отправить
        current_dir = os.path.abspath(os.path.dirname('test_2_2_3_sendfile'))
        print(current_dir)
        file_path = os.path.join(current_dir, 'test.txt')
        #ижу кнопку прикрепления файла и отправляю текстовый файл
        input_file = driver.find_element_by_id('file')
        input_file.send_keys(file_path)
        #ищу кнопку submit и жму на нее
        driver.find_element_by_xpath('//button').click()

    finally:
        time.sleep(7)
        driver.quit()