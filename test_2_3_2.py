import os
from selenium.webdriver.chrome.webdriver import WebDriver
import time
import math
from selenium.webdriver.support.select import Select


def test_2_2_3_sendfile():
    link = 'http://suninjuly.github.io/redirect_accept.html'  # url для перехода
    try:
        driver = WebDriver(executable_path='C://Users//Tom//Documents//WebUI//chromedriver.exe')
        driver.get(link)

        driver.find_element_by_xpath('//button').click()
        #назначаю переменной вторую вкладку и переключаюсь на нее
        new_window = driver.window_handles[1]
        driver.switch_to.window(new_window)

        #расчет формулы
        x = driver.find_element_by_id('input_value').text
        def calc(x):
            return str(math.log(abs(12 * math.sin(int(x)))))
        y = calc(x)
        driver.find_element_by_id('answer').send_keys(y)
        driver.find_element_by_xpath('//button').click()

        #получаю ответ с алерта
        alert = driver.switch_to.alert
        alert_text = alert.text

        print(alert_text)



    finally:
        time.sleep(5)
        driver.quit()