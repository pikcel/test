from selenium.webdriver.chrome.webdriver import WebDriver
import time
import math

from selenium.webdriver.support.select import Select


def test_2_2_1():
    link = 'http://suninjuly.github.io/selects1.html'  # url для перехода
    try:
        driver = WebDriver(executable_path='C://Users//Tom//Documents//WebUI//chromedriver.exe')
        driver.get(link)

        x = driver.find_element_by_id('num1').text
        y = driver.find_element_by_id('num2').text

        summ = int(x) + int(y)

        select = Select(driver.find_element_by_id('dropdown'))
        select.select_by_value(str(summ))

        driver.find_element_by_xpath('//button').click()



    finally:
        time.sleep(7)
        driver.quit()