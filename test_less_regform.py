from selenium.webdriver.chrome.webdriver import WebDriver
import time

def test_less_regform():
    link = 'http://suninjuly.github.io/registration1.html'  # url для перехода
    try:
        driver = WebDriver(executable_path='C://Users//Tom//Documents//WebUI//chromedriver.exe')
        driver.get(link)
        # заполняем обязательные поля формы
        input1 = driver.find_element_by_xpath('//input[contains(@placeholder,"Input your first name")]')
        input1.send_keys('First name')

        input2 = driver.find_element_by_xpath('//input[contains(@placeholder,"Input your last name")]')
        input2.send_keys('Last name')

        input3 = driver.find_element_by_xpath('//input[contains(@placeholder,"Input your email")]')
        input3.send_keys('test@mail.com')

        #Отправляем форму
        button = driver.find_element_by_tag_name('button')
        button.click()

        #Ждем немного и проверяем получили ли мы текст Congratulation
        time.sleep(5)

        #Находим нужный текст и сравниваем
        assert "Congratulations! You have successfully registered!" == driver.find_element_by_xpath('//h1').text
        print('Test passed, registration successful')

    finally:
        time.sleep(5)
        driver.quit()