from selenium.webdriver.chrome.webdriver import WebDriver
import time
import math

def test_2_1_4():
    link = 'http://suninjuly.github.io/get_attribute.html'  # url для перехода
    try:
        driver = WebDriver(executable_path='C://Users//Tom//Documents//WebUI//chromedriver.exe')
        driver.get(link)

        x_element = driver.find_element_by_id('treasure')
        x_atr = x_element.get_attribute('valuex')
        x = x_atr

        def calc(x):
            return str(math.log(abs(12 * math.sin(int(x)))))
        y = calc(x)

        input = driver.find_element_by_id('answer')
        input.send_keys(y)

        #отмечаю чекбокс и радиобаттон, жму submit
        driver.find_element_by_id('robotCheckbox').click()
        driver.find_element_by_id('robotsRule').click()
        driver.find_element_by_xpath('//button').click()


    finally:
        time.sleep(5)
        driver.quit()