from selenium.webdriver.chrome.webdriver import WebDriver
import time

def test_less1_6_4():
    link = 'http://suninjuly.github.io/find_xpath_form'  # url для перехода
    try:
        driver = WebDriver(executable_path='C://Users//Tom//Documents//WebUI//chromedriver.exe')
        driver.get(link)
        # заполняем инпуты
        input1 = driver.find_element_by_name('first_name')
        input1.send_keys('First name')

        input2 = driver.find_element_by_name('last_name')
        input2.send_keys('Last name')

        input3 = driver.find_element_by_name('firstname')
        input3.send_keys('City')

        input4 = driver.find_element_by_id('country')
        input4.send_keys('Country')
        #Ищем нужную кнопку и кликаем
        button = driver.find_element_by_xpath('//button[text()="Submit"]')
        button.click()


    finally:
        time.sleep(15)
        driver.quit()