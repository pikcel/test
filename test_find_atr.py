from selenium.webdriver.chrome.webdriver import WebDriver
import time
import math

def test_find_atr():
    link = 'http://suninjuly.github.io/math.html'  # url для перехода
    try:
        driver = WebDriver(executable_path='C://Users//Tom//Documents//WebUI//chromedriver.exe')
        driver.get(link)

        #создаю переменную для поиска нужного радиобаттона
        radio_button = driver.find_element_by_xpath('//input[@value="robots"]')
        #создал переменную которая вытянет атрибут из радиобаттона
        radio_checked = radio_button.get_attribute('checked')
        #сравниваю есть ли в радиобаттоне атрибут checked, если нет вывожу сообщение и тест должен пройти как failed
        assert radio_checked == 'true', 'People radio is not selected by default'

    finally:
        time.sleep(5)
        driver.quit()