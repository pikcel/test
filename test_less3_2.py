from selenium.webdriver.chrome.webdriver import WebDriver
import time
import unittest

class TestFirst(unittest.TestCase):
    def test_reg2(self):
        try:
            driver = WebDriver(executable_path='C://Users//Tom//Documents//WebUI//chromedriver.exe')
            link2 = 'http://suninjuly.github.io/registration2.html'
            driver.get(link2)

            # Заполняю форму регистрации
            input1 = driver.find_element_by_css_selector('div.first_block input.first')
            input1.send_keys('First name')
            input2 = driver.find_element_by_css_selector('.first_block input.second')
            input2.send_keys('Last name')
            input3 = driver.find_element_by_css_selector('.first_block input.third')
            input3.send_keys('mail@mail.com')

            # Отправляем заполненную форму
            button = driver.find_element_by_xpath('//button[@type="submit"]')
            button.click()

            # Проверяем что смогли зарегестрироваться и ждем
            time.sleep(2)

            # Находим элемент, содержащий текст
            welcome_text_elt = driver.find_element_by_tag_name('h1')
            # Записываем текст в переменнную
            welcome_text = welcome_text_elt.text
            expected_result = 'Congratulations! You have successfully registered!'

            self.assertEqual('Congratulations! You have successfully registered!', welcome_text, 'Text does not much')

        finally:
            # Ожидаем для простого сравнения
            time.sleep(1)
            # Закрываем браузер
            driver.quit()

    def test_reg1(self):
        try:
            driver = WebDriver(executable_path='C://Users//Tom//Documents//WebUI//chromedriver.exe')
            link1 = 'http://suninjuly.github.io/registration1.html'
            driver.get(link1)

            # Заполняю форму регистрации
            input1 = driver.find_element_by_css_selector('div.first_block input.first')
            input1.send_keys('First name')
            input2 = driver.find_element_by_css_selector('.first_block input.second')
            input2.send_keys('Last name')
            input3 = driver.find_element_by_css_selector('.first_block input.third')
            input3.send_keys('mail@mail.com')

            # Отправляем заполненную форму
            button = driver.find_element_by_xpath('//button[@type="submit"]')
            button.click()

            # Проверяем что смогли зарегестрироваться и ждем
            time.sleep(2)

            # Находим элемент, содержащий текст
            welcome_text_elt = driver.find_element_by_tag_name('h1')
            # Записываем текст в переменнную
            welcome_text = welcome_text_elt.text
            expected_result = 'Congratulations! You have successfully registered!'

            self.assertEqual('Congratulations! You have successfully registered!', welcome_text, 'Text does not much')

        finally:
            # Ожидаем для простого сравнения
            time.sleep(3)
            # Закрываем браузер
            driver.quit()



if __name__ == '__main__':
    unittest.main()
