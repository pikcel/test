from selenium.webdriver.chrome.webdriver import WebDriver
import time
import math

def test_less2_1_3():
    link = 'http://suninjuly.github.io/math.html'  # url для перехода
    try:
        driver = WebDriver(executable_path='C://Users//Tom//Documents//WebUI//chromedriver.exe')
        driver.get(link)

        #создаем переменную Х и беру значение на странице, создаем переменную для расчета
        x_element = driver.find_element_by_id('input_value')
        x = x_element.text
        def calc(x):
            return str(math.log(abs(12 * math.sin(int(x)))))
        y = calc(x)
        #Вводим в инпут ответ с формулы, переменная y
        input1 = driver.find_element_by_id('answer')
        input1.send_keys(y)

        #проставляем чекбокс,радиобаттон и жмем submit
        driver.find_element_by_css_selector('[for="robotCheckbox"]').click()
        driver.find_element_by_css_selector('[for="robotsRule"]').click()
        driver.find_element_by_xpath('//button').click()


    finally:
        time.sleep(5)
        driver.quit()