from selenium.webdriver.chrome.webdriver import WebDriver
import time
import math
from selenium.webdriver.support.select import Select


def test_2_2_2_js():
    link = 'http://suninjuly.github.io/execute_script.html'  # url для перехода
    try:
        driver = WebDriver(executable_path='C://Users//Tom//Documents//WebUI//chromedriver.exe')
        driver.get(link)

        #расчет формулы
        x = driver.find_element_by_id('input_value').text
        def calc(x):
            return str(math.log(abs(12 * math.sin(int(x)))))
        y = calc(x)
        input1 = driver.find_element_by_id('answer')
        input1.send_keys(y)

        # скроллим страницу внизу на n-pixel
        driver.execute_script('window.scrollBy(0, 200);')
        # ставлю чекбокс и радиобаттон, без переменных
        driver.find_element_by_css_selector('[for="robotCheckbox"]').click()
        driver.find_element_by_css_selector('[for="robotsRule"]').click()
        #ищу кнопку и жмякаю
        driver.find_element_by_xpath('//button').click()

    finally:
        time.sleep(7)
        driver.quit()