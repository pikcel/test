from selenium.webdriver.chrome.webdriver import WebDriver
import time

def test_main():
    link = 'http://suninjuly.github.io/simple_form_find_task.html'  # url для перехода
    try:
        driver = WebDriver(executable_path='C://Users//Tom//Documents//WebUI//chromedriver.exe')
        driver.get(link)
        input1 = driver.find_element_by_name('first_name')
        input1.send_keys('First name')

        input2 = driver.find_element_by_name('last_name')
        input2.send_keys('Last name')

        input3 = driver.find_element_by_class_name('city')
        input3.send_keys('Kyiv')

        input4 = driver.find_element_by_id('country')
        input4.send_keys('Ukraine')

        button = driver.find_element_by_id('submit_button')
        button.click()

    finally:
        time.sleep(5)
        driver.quit()